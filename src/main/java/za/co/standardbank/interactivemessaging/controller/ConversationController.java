package za.co.standardbank.interactivemessaging.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import za.co.standardbank.interactivemessaging.helper.JsonConverter;
import za.co.standardbank.interactivemessaging.helper.ConstantFile.ApplicationConstant;
import za.co.standardbank.interactivemessaging.model.amqp.Messages;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoConversation;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.entity.Message;
import za.co.standardbank.interactivemessaging.model.enums.AutoResponse;
import za.co.standardbank.interactivemessaging.model.enums.ConversationStatus;
import za.co.standardbank.interactivemessaging.model.request.ReallocateConversation;
import za.co.standardbank.interactivemessaging.model.response.JsonResponse;
import za.co.standardbank.interactivemessaging.repository.ConversationRepository;
import za.co.standardbank.interactivemessaging.repository.InternalUserRepository;
import za.co.standardbank.interactivemessaging.service.ConversationService;
import za.co.standardbank.interactivemessaging.service.MessageService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class ConversationController 
{
    @Autowired
    private ConversationService _conversationService;

    @Autowired
    private MessageService _messageService;

    @Autowired
    private ConversationRepository _conversationRepository;

    @Autowired
    private InternalUserRepository _internalUserRepository;

    @Autowired
    private JsonResponse _jsonResponse;

    @Autowired
    private JsonConverter _jsonConverter;

    private final Logger logger = LoggerFactory.getLogger(ConversationController.class);

    // Getting Conversation History for a specific customer
    @GetMapping("api/get/conversations/history/{cus_id}")
    public ResponseEntity<?> getConversationsById(@PathVariable("cus_id") final Long customerId)
    {
        try{

            if(customerId == null)
            {
                _jsonResponse.setMessage("customerId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            final List<DtoConversation> dtoConversations = new ArrayList<>();
            final List<Conversation> conversations = _conversationRepository.findAllConversationsByCustomer_CustomerId(customerId);

            if(conversations == null || conversations.isEmpty())
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            for (final Conversation conversation : conversations) 
            {
                dtoConversations.add(new DtoConversation(conversation));
            }

            return new ResponseEntity<>(dtoConversations, HttpStatus.OK);
        }
        catch (final Exception e)
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Getting all messages for an conversation of a customer
    @GetMapping("api/get/conversation/messages/{cus_id}")
    public ResponseEntity<?> getConversationMessages(@PathVariable("cus_id") final Long customerId)
    {
        try 
        {
            List<Conversation> conversations = new ArrayList<>();
            final List<DtoConversation> dtoConversations = new ArrayList<>();

            if(customerId == null)
            {
                _jsonResponse.setMessage("customerId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            conversations = _conversationRepository.findAllConversationByCustomer_CustomerIdAndConversationStatusOrCustomer_CustomerIdAndConversationStatus(customerId, ConversationStatus.ACTIVE.getValue(), customerId, ConversationStatus.NEW.getValue());
            
            if(conversations == null || conversations.isEmpty())
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            for (final Conversation conversation : conversations) 
            {                      
                List<Message> msgs = new ArrayList<>();
                msgs = _conversationService.initRecursion(conversation);

                _conversationService.SetAllMessagestoReadforConversation(conversation.conversationId);
                
                if(!msgs.equals(null) || !msgs.isEmpty()){
                    
                    for(Message msg : msgs){
                        conversation.messages.add(msg);
                    }                    
                }
                
                dtoConversations.add(new DtoConversation(conversation));    
            }

            return new ResponseEntity<>(dtoConversations, HttpStatus.OK);
        } 
        catch (final Exception e) 
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    } 

    @GetMapping("api/get/conversation/{conversation_id}")
    public ResponseEntity<?> getConversationById(@PathVariable("conversation_id") final Long conversationId)
    {
        try 
        {
            if(conversationId == null)
            {
                _jsonResponse.setMessage("conversationId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            final Conversation conversation = _conversationRepository.findConversationByConversationId(conversationId);

            if(conversation == null)
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            final DtoConversation dtoConversation = new DtoConversation(conversation);

            return new ResponseEntity<>(dtoConversation, HttpStatus.OK);

        } 
        catch (final Exception e) 
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("api/get/conversations/{conversation_id}")
    public ResponseEntity<?> getConversationsByConversationId(@PathVariable("conversation_id") final Long conversationId)
    {
        try 
        {
            if(conversationId == null)
            {
                _jsonResponse.setMessage("conversationId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            final Conversation conversation = _conversationRepository.findConversationByConversationId(conversationId);

            if(conversation == null)
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            List<Message> msgs = new ArrayList<>();

            msgs = _conversationService.initRecursion(conversation);

            if(!msgs.equals(null) || !msgs.isEmpty()){
                
                for(Message msg : msgs){
                    conversation.messages.add(msg);
                }            
            }

            final DtoConversation dtoConversation = new DtoConversation(conversation);

            return new ResponseEntity<>(dtoConversation, HttpStatus.OK);

        } 
        catch (final Exception e) 
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }




    @PutMapping("api/update/conversation/escalate/{conversation_id}")
    public ResponseEntity<?> escalateConversation(@PathVariable("conversation_id") final Long conversationId)
    {
        try
        {
            if(conversationId == null)
            {
                _jsonResponse.setMessage("conversationId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            final Conversation conversation = _conversationService.escalateConversation(conversationId);

            if(conversation == null)
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            final DtoConversation dtoConversation = new DtoConversation(conversation);

            return new ResponseEntity<>(dtoConversation, HttpStatus.OK);
        }
        catch(final Exception e)
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("api/update/conversation/close/{conversation_id}")
    public ResponseEntity<?> closeConversation(@PathVariable("conversation_id") final Long conversationId)
    {
        try
        {
            if(conversationId == null)
            {
                _jsonResponse.setMessage("conversationId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            final Conversation conversation = _conversationService.closeConversation(conversationId);

            if(conversation == null)
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            final DtoConversation dtoConversation = new DtoConversation(conversation);
            _messageService.sendAutomatedMessage(conversation, AutoResponse.CLOSED);

            return new ResponseEntity<>(dtoConversation, HttpStatus.OK);
        }
        catch(final Exception e)
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("api/get/conversations/active/{username}")
    public ResponseEntity<?> retrieveActiveConversationsByTB(@PathVariable("username") final String username) 
    {
        logger.info("Get set of active and new conversations by TB username");
        try 
        {
            final List<Conversation> conversations = _conversationService.getActiveConversationForInternalUser(username);
            final List<DtoConversation> dtoConversations = new ArrayList<>();

            if (conversations == null) 
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            for (final Conversation conversation : conversations) 
            {
                dtoConversations.add(new DtoConversation(conversation));
            }

            return new ResponseEntity<>(dtoConversations, HttpStatus.OK);
        } 
        catch (final Exception e) 
        {
            logger.error("The following exception occurred", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("api/update/conversation")
    public ResponseEntity<?> referReallocateConversation(@RequestBody final ReallocateConversation request) 
    {
        logger.info("Refer or reallocate conversation to another TB");
        try 
        {
            DtoConversation dtoConversation;
            final InternalUser internalUser = _internalUserRepository.findByUsername(request.username);

            if (internalUser == null) 
            {
                _jsonResponse.setMessage("internalUser " + ApplicationConstant.NotFound);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NOT_FOUND);
            }

            final Conversation conversation = _conversationRepository.findConversationByConversationId(request.conversationId);

            if (conversation == null) 
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            if (request.reallocateType.equals("refer")) 
            {
                final Conversation referredConversation = _conversationService.referConversation(request.getConversationId());

                _messageService.sendAutomatedMessage(referredConversation, AutoResponse.REFER);

                dtoConversation = new DtoConversation(referredConversation);

                return new ResponseEntity<>(dtoConversation, HttpStatus.OK);
            }
            else if (request.reallocateType.equals("reallocate")) 
            {
                final Conversation referredConversation = _conversationService.reallocateAndOpenConversation(conversation, internalUser, ConversationStatus.REALLOCATED);

                dtoConversation = new DtoConversation(referredConversation);

                return new ResponseEntity<>(dtoConversation, HttpStatus.OK);
            }

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } 
        catch (final Exception e) 
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("api/get/conversations/escalated")
    public ResponseEntity<?> getEscalatedConversations()
    {
        List<Conversation> conversations = new ArrayList<>();
        final List<DtoConversation> dtoConversations = new ArrayList<>();

        try
        {
            conversations = _conversationService.getAllEscalatedConversations();

            if(conversations == null || conversations.isEmpty())
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            for (final Conversation conversation : conversations) 
            {
                dtoConversations.add(new DtoConversation(conversation));
            }

            return new ResponseEntity<>(dtoConversations, HttpStatus.OK);
        } 
        catch (final Exception e) 
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }
}