package za.co.standardbank.interactivemessaging.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import za.co.standardbank.interactivemessaging.helper.JsonConverter;
import za.co.standardbank.interactivemessaging.helper.ConstantFile.ApplicationConstant;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.entity.Message;
import za.co.standardbank.interactivemessaging.model.enums.ConversationStatus;
import za.co.standardbank.interactivemessaging.model.request.NewMessage;
import za.co.standardbank.interactivemessaging.model.response.JsonResponse;
import za.co.standardbank.interactivemessaging.repository.*;
import za.co.standardbank.interactivemessaging.service.MessageService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class MessageController {

    @Autowired
    private MessageService _messageService;

    @Autowired
    private InternalUserRepository _internalUserRepository;

    @Autowired
    private MessageRepository _messageRepository;

    @Autowired
    private ConversationRepository _conversationRepository;
    
    @Autowired
    private JsonResponse _jsonResponse;

    @Autowired
    private JsonConverter _jsonConverter;

    private Logger logger = LoggerFactory.getLogger(MessageController.class);

    @GetMapping("api/get/messages/{msg_sender_id}")
    public ResponseEntity<?> getMessagesBySenderId(@PathVariable("msg_sender_id") String senderId)
    {
        try
        {
            List<Message> messages = new ArrayList<Message>();

            if(senderId == null)
            {
                _jsonResponse.setMessage("customerId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }
    
            messages = _messageRepository.findAllMessagesBySenderId(senderId);
            
            return new ResponseEntity<>(messages, HttpStatus.OK);
        } 
        catch (Exception e)
        {
            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping("api/post/send_message")
    public ResponseEntity<?> postOutgoingMessage(@RequestBody NewMessage newMessage) {
        logger.info("Post new message to customer");

        try 
        {
            InternalUser internalUser = _internalUserRepository.findByUsername(newMessage.getUsername());

            if (internalUser == null) 
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            Conversation conversation = _conversationRepository.findConversationByConversationId(newMessage.getConversationId());

            if (conversation == null) 
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            if(conversation.conversationStatus.equals(ConversationStatus.NEW.getValue())){
                conversation.conversationStatus = ConversationStatus.ACTIVE.getValue();
                _conversationRepository.save(conversation);
            }            

            Message postedMessage = _messageService.postNewMessage(newMessage, conversation);

            _messageRepository.save(postedMessage);
            _messageService.mapWhatsappMessage(postedMessage.messageText, conversation);

            return new ResponseEntity<>(postedMessage, HttpStatus.OK);

        } catch (Exception e) 
        {
            logger.error("Following exception occured", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }
}
