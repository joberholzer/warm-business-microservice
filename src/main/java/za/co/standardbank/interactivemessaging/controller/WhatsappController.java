package za.co.standardbank.interactivemessaging.controller;

import org.springframework.web.bind.annotation.RequestBody;
import za.co.standardbank.interactivemessaging.config.ApplicationConfigReader;
import za.co.standardbank.interactivemessaging.helper.ConstantFile.ApplicationConstant;
import za.co.standardbank.interactivemessaging.model.amqp.SendWhatsapp;
import za.co.standardbank.interactivemessaging.rabbitmq.MessageSender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class WhatsappController {

    private static final Logger log = LoggerFactory.getLogger(WhatsappController.class);
  
    @Autowired
    private final RabbitTemplate rabbitTemplate;
  
    @Autowired
    private ApplicationConfigReader applicationConfig;
  
    @Autowired
      private za.co.standardbank.interactivemessaging.rabbitmq.MessageSender messageSender;
  
    public ApplicationConfigReader getApplicationConfig() 
    {
          return applicationConfig;
     }
  
    @Autowired
    public void setApplicationConfig(ApplicationConfigReader applicationConfig) 
    {
          this.applicationConfig = applicationConfig;
    }
      
    @Autowired
    public WhatsappController(final RabbitTemplate rabbitTemplate) 
    {
          this.rabbitTemplate = rabbitTemplate;
    }
      
    public MessageSender getMessageSender() 
    {
          return messageSender;
    }
      
    @Autowired
    public void setMessageSender(MessageSender messageSender) 
    {
      this.messageSender = messageSender;
    }

    @PostMapping("api/send/message")
    public ResponseEntity<?> SendToWhatsappNew(@RequestBody SendWhatsapp sendMessageCall)
    {
      String exchange = getApplicationConfig().getToWhatsappExchange();
      String routingKey = getApplicationConfig().getToWhatsappRoutingKey();

      /* Sending to Message Queue */
      try 
      {
      messageSender.sendMessage(rabbitTemplate, exchange, routingKey, sendMessageCall);

      return new ResponseEntity<String>(ApplicationConstant.IN_QUEUE, HttpStatus.OK);   
      } 
      catch (Exception ex) 
      {
      log.error("Exception occurred while sending message to the queue. Exception= {}", ex);

      return new ResponseEntity(ApplicationConstant.MESSAGE_QUEUE_SEND_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } 
}