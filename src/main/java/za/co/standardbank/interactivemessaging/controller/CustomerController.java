package za.co.standardbank.interactivemessaging.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import za.co.standardbank.interactivemessaging.model.entity.Customer;
import za.co.standardbank.interactivemessaging.model.request.USSDRequest;
import za.co.standardbank.interactivemessaging.model.response.JsonResponse;
import za.co.standardbank.interactivemessaging.helper.JsonConverter;
import za.co.standardbank.interactivemessaging.helper.ConstantFile.ApplicationConstant;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoCustomer;
import za.co.standardbank.interactivemessaging.repository.CustomerRepository;
import za.co.standardbank.interactivemessaging.service.CustomerService;


@RestController
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService _customerService;

    @Autowired
    private CustomerRepository _customerRepository;

    private Logger logger = LoggerFactory.getLogger(CustomerController.class); 
    

    @Autowired
    private JsonResponse _jsonResponse;

    @Autowired
    private JsonConverter _jsonConverter;

    @GetMapping("api/get/customer/{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable("customerId") Long customerId) 
    {
        logger.info("Get Customer details");

        try 
        {
            if(customerId == null)
            {
                _jsonResponse.setMessage("customerId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            Customer customer = _customerRepository.findCustomersByCustomerId(customerId);

            if(customer == null){

                _jsonResponse.setMessage(ApplicationConstant.NotFound);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(_customerService.convertCustomer(customer), HttpStatus.OK);

        } 
        catch (Exception e) 
        {
            logger.error("The following exception occurred", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping("api/post/register/opt")
    public ResponseEntity<?> registerCustomer(@RequestBody USSDRequest request) 
    {
        logger.info("Opt-out for whats-app banking service....");

        try 
        {
           if(!_customerService.checkCustomerQualify(request))
           {
            _jsonResponse.setMessage(ApplicationConstant.Forbidden);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.FORBIDDEN);
           }

           Customer existingCustomer = _customerRepository.findCustomerByMobileNumber(request.getCellphoneNumber());

           if(request.isOptInorOut() == true && existingCustomer.customerOptStatus.equals(true)){
            
                _jsonResponse.setMessage(ApplicationConstant.CustomerAlreadyRegistered);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.OK);
           }
           else if(request.isOptInorOut() == false && existingCustomer.customerOptStatus.equals(false)){

                _jsonResponse.setMessage(ApplicationConstant.CustomerAlreadyDeregistered);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.OK);
           }

            Customer customer = _customerService.registerCustomer(request);

            if(customer.customerOptStatus.equals(true)){
                _jsonResponse.setMessage(ApplicationConstant.CustomerRegisteredSuccessfully);
            }
            else{
                _jsonResponse.setMessage(ApplicationConstant.CustomerDeregisteredSuccessfully);
            }
            
            return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse), HttpStatus.OK);
        } 
        catch (Exception e) 
        {
            logger.error("The following exception occurred: ", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping("api/post/check/qualify")
    public ResponseEntity<String> customerCheck(@RequestBody USSDRequest request)
    {
        logger.info("Check if the customer qualifies....");
        
        try 
        {
            if(!_customerService.checkCustomerQualify(request))
            {
             _jsonResponse.setMessage(ApplicationConstant.Forbidden);
 
             return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.FORBIDDEN);
            }

            _jsonResponse.setMessage("User Qualifies to optIn");
            
            return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.OK);
        } 
        catch (Exception e) 
        {
            logger.error("The following exception occurred: ", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("api/get/customers/{portflio_id}")
    public ResponseEntity<?> getCustomersByPortfolioId(@PathVariable("portflio_id") Long portfolioId)
    {
        try
        {
            if(portfolioId == null)
            {
                _jsonResponse.setMessage("PortfolioId " + ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            List<Customer> customers = _customerRepository.findAllCustomersByPortfolio_PortfolioId(portfolioId);
            List<DtoCustomer> dtoCustomers = new ArrayList<>();

            if(customers == null || customers.isEmpty())
            {
                _jsonResponse.setMessage(ApplicationConstant.NoContent);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.NO_CONTENT);
            }

            for (Customer customer : customers) {
                dtoCustomers.add(new DtoCustomer(customer));
            }

            return new ResponseEntity<>(dtoCustomers, HttpStatus.OK);
        } 
        catch (Exception e)
        {
            logger.error("Following exception occured", e);
            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);
        }
    }
}