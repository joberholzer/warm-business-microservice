package za.co.standardbank.interactivemessaging.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.standardbank.interactivemessaging.helper.ConstantFile;
import za.co.standardbank.interactivemessaging.helper.JsonConverter;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoConversationUpdate;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoInternalUser;
import za.co.standardbank.interactivemessaging.model.enums.ConversationStatus;
import za.co.standardbank.interactivemessaging.model.enums.InternalUserType;
import za.co.standardbank.interactivemessaging.model.response.JsonResponse;
import za.co.standardbank.interactivemessaging.repository.ConversationRepository;
import za.co.standardbank.interactivemessaging.repository.InternalUserRepository;
import za.co.standardbank.interactivemessaging.service.ConversationService;
import za.co.standardbank.interactivemessaging.service.InternalUserService;
import za.co.standardbank.interactivemessaging.service.MessageService;

import java.util.ArrayList;
import java.util.List;


@RestController
@CrossOrigin
public class InternalUserController {

    @Autowired
    private ConversationService _conversationService;

    @Autowired
    private InternalUserService _internalUserService;

    @Autowired
    private InternalUserRepository _internalUserRepository;

    @Autowired
    private ConversationRepository _conversationRepository;

    @Autowired
    private JsonResponse _jsonResponse;

    @Autowired
    private JsonConverter _jsonConverter;

    @Autowired
    private MessageService _messageService;

    private Logger logger = LoggerFactory.getLogger(InternalUserController.class);


    @PutMapping("api/update/user/deactivatetb/{username}")
    public ResponseEntity<InternalUser> deactivateTB(@PathVariable("username") String username) {

        logger.info("Deactivate a TB and close all their conversations");

        try {
            
            if (username == null) {
                logger.error("The following exception occurred");

                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            InternalUser internalUser = _internalUserRepository.findByUsername(username);

            List<InternalUser> internalUsers = _internalUserRepository.findAllInternalUsersByStatusAndUserType_userTypeNameAndTeam_TeamId(true, InternalUserType.TRANSACTIONAL_BANKER.getValue(), internalUser.team.teamId);

            for (InternalUser internaluser : internalUsers) {        
                List<Conversation> existingConversations = _conversationRepository.findAllConversationsByInternalUser_UsernameAndConversationStatusOrInternalUser_UsernameAndConversationStatus(
                    internaluser.username, ConversationStatus.ACTIVE.getValue(), internaluser.username, ConversationStatus.NEW.getValue());
    
                internaluser.conversations.addAll(existingConversations);
            }

            List<Conversation> conversations = _conversationRepository.findConversationsByInternalUser_Username(username);

            internalUser = _internalUserService.deactivateUser(internalUser);
            _internalUserRepository.save(internalUser);
            
            for (Conversation conversation : conversations) {
                
                Conversation newConversation = new Conversation();

                conversation.conversationStatus = ConversationStatus.CLOSED.getValue();

                InternalUser availableInternalUser = _messageService.getInternalUserCount(internalUsers);
                
                newConversation.conversationParentId = conversation.conversationId;
                newConversation.customer = conversation.customer;
                newConversation.internalUser = availableInternalUser;
                newConversation.conversationStatus = ConversationStatus.NEW.getValue();

                _conversationRepository.save(conversation);
                _conversationRepository.save(newConversation);                
            }

            return new ResponseEntity<>(internalUser, HttpStatus.OK);

        } catch (Exception e) {
            logger.error("The following exception occurred", e);

            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    //Get a list of all Transactional Bankers Per Team
    @GetMapping("api/get/transactionalbankers/{team_id}")
    public ResponseEntity<List<DtoInternalUser>> getTransactionalBankersPerTeam(@PathVariable("team_id") Long teamId ) {

        try {

            logger.info("Retrieve a list of all Transactional Bankers per Team based on TeamId...");

            List<DtoInternalUser> dtoInternalUsers = new ArrayList<>();

            if (teamId  == null) {

                logger.error("No Transactional Banker(s) Found wih TeamId: "+  teamId);

                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);

            }

            List<InternalUser> internalUsers = _internalUserRepository.findAllInternalUsersByUserType_userTypeNameAndTeam_TeamId(InternalUserType.TRANSACTIONAL_BANKER.getValue(), teamId);

            if(internalUsers == null || internalUsers.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            for (InternalUser internalUser : internalUsers) {
                dtoInternalUsers.add(new DtoInternalUser(internalUser));
            }

            return new ResponseEntity<>(dtoInternalUsers, HttpStatus.OK);

        } catch (Exception e) {

            logger.error("The following exception occurred: ", e);

            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);

        }
    }

    //Get all the information of a specific internalUser by making use of their username being the a/c number
    @GetMapping("api/get/internaluser/{username}")
    public ResponseEntity<?> getInternalUserInformation(@PathVariable("username") String username) {

        try {

            if(username == null || username.isEmpty())
            {

                _jsonResponse.setMessage("username " + ConstantFile.ApplicationConstant.CanNotBeNull);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.BAD_REQUEST);
            }

            logger.info("Retrieving InternalUser Information for the InternalUser with username: "+ username);

            InternalUser internalUser = _internalUserRepository.findByUsername(username);

            if(internalUser == null)
            {

                _jsonResponse.setMessage(ConstantFile.ApplicationConstant.NotFound);

                return new ResponseEntity<>(_jsonConverter.JsonConvert(_jsonResponse),HttpStatus.UNAUTHORIZED);
            }

            return new ResponseEntity<>(internalUser, HttpStatus.OK);

        } catch (Exception e) {

            logger.error("The following exception occurred: ", e);

            return new ResponseEntity<>(_jsonConverter.JsonConvert(e.getMessage()),HttpStatus.EXPECTATION_FAILED);

        }
    }

    //Get a list of all team leads
    @GetMapping("api/get/teamleads")
    public ResponseEntity<?> getAllTeamLeads() {
        
        try {
            
            logger.info("Retrieve a list of all Team Leads");
            
            List<DtoInternalUser> dtoInternalUsers = new ArrayList<>();
            List<InternalUser> internalUsers = _internalUserRepository.findAllInternalUsersByUserType_userTypeName(InternalUserType.TEAM_LEAD.getValue());
            
            if(internalUsers == null || internalUsers.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            
            for (InternalUser internalUser : internalUsers) {
                dtoInternalUsers.add(new DtoInternalUser(internalUser));
            }
            
            return new ResponseEntity<>(dtoInternalUsers, HttpStatus.OK);
        
        } catch (Exception e) {
            logger.error("The following exception occurred: ", e);
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}
