package za.co.standardbank.interactivemessaging.controller;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.standardbank.interactivemessaging.service.MessageService;
import za.co.standardbank.interactivemessaging.service.PortfolioService;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoPortfolio;
import za.co.standardbank.interactivemessaging.model.entity.Portfolio;
import za.co.standardbank.interactivemessaging.repository.PortfolioRepository;



@RestController
@CrossOrigin
public class PortfolioController {

    @Autowired
    private PortfolioService _portfolioService;

    private Logger logger = LoggerFactory.getLogger(PortfolioController.class);

    @Autowired
    private PortfolioRepository _portfolioRepository;
    
    @GetMapping("api/get/portfolios/{username}")
    public ResponseEntity<List<Portfolio>> getPortfoliosByTbId(@PathVariable("username") String username){
        
        try{

            if(username == null)
            {
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }

            List<Portfolio> portfolios = _portfolioService.getPortfolioByTb(username);

            if(portfolios == null || portfolios.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(portfolios, HttpStatus.OK);
        } 
        catch (Exception e){
            logger.error("Following exception occured", e);
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("api/get/portfolios")
    public ResponseEntity<?> getPortfoliosByTbId(){
        
        try{
            
            List<Portfolio> portfolios = _portfolioRepository.findAll();
            List<DtoPortfolio> dtoPortfolios = new ArrayList<>();

            if(portfolios == null || portfolios.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            for (Portfolio portfolio : portfolios) {

                dtoPortfolios.add(new DtoPortfolio(portfolio));
            }

            return new ResponseEntity<>(dtoPortfolios, HttpStatus.OK);
        } 
        catch (Exception e){
            logger.error("Following exception occured", e);
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}
