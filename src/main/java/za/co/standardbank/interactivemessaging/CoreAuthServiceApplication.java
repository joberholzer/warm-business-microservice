package za.co.standardbank.interactivemessaging;

import za.co.standardbank.interactivemessaging.config.ApplicationConfigReader;
import za.co.standardbank.interactivemessaging.helper.JsonConverter;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableRabbit
@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
@EnableJpaAuditing
@EnableJpaRepositories(basePackages="za.co.standardbank.interactivemessaging")
@EnableScheduling
public class CoreAuthServiceApplication extends SpringBootServletInitializer implements RabbitListenerConfigurer {

	@Autowired
	private ApplicationConfigReader applicationConfig;
	
	public ApplicationConfigReader getApplicationConfig() {
		return applicationConfig;
	}
	
	public void setApplicationConfig(ApplicationConfigReader applicationConfig) {
		this.applicationConfig = applicationConfig;
	}

	public static void main(String[] args) {

		SpringApplication.run(CoreAuthServiceApplication.class, args);
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CoreAuthServiceApplication.class);
	}

		/* This bean is to read the properties file configs */	
		@Bean
		public ApplicationConfigReader applicationConfig() {
			return new ApplicationConfigReader();
		}

		@Bean JsonConverter jsonConverter(){
			return new JsonConverter();
		}
		
		/* Creating a bean for the Message queue Exchange */
		@Bean
		public TopicExchange getFromWhatsappExchange() {
			return new TopicExchange(getApplicationConfig().getFromWhatsappExchange());
		}
	
		/* Creating a bean for the Message queue */
		@Bean
		public Queue getFromWhatsappQueue() {
			return new Queue(getApplicationConfig().getFromWhatsappQueue());
		}
		
		/* Binding between Exchange and Queue using routing key */
		@Bean
		public Binding declareBindingFromWhatsapp() {
			return BindingBuilder.bind(getFromWhatsappQueue()).to(getFromWhatsappExchange()).with(getApplicationConfig().getFromWhatsappRoutingKey());
		}
		
		/* Creating a bean for the Message queue Exchange */
		@Bean
		public TopicExchange getToWhatsappExchange() {
			return new TopicExchange(getApplicationConfig().getToWhatsappExchange());
		}
	
		/* Creating a bean for the Message queue */
		@Bean
		public Queue getToWhatsappQueue() {
			return new Queue(getApplicationConfig().getToWhatsappQueue());
		}
		
		/* Binding between Exchange and Queue using routing key */
		@Bean
		public Binding declareBindingToWhatsapp() {
			return BindingBuilder.bind(getToWhatsappQueue()).to(getToWhatsappExchange()).with(getApplicationConfig().getToWhatsappRoutingKey());
		}
	
		/* Bean for rabbitTemplate */
		@Bean
		public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
			final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
			rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
			return rabbitTemplate;
		}
	
		@Bean
		public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
			return new Jackson2JsonMessageConverter();
		}
	
		@Bean
		public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
			return new MappingJackson2MessageConverter();
		}
		
		@Bean
		public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
			DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
			factory.setMessageConverter(consumerJackson2MessageConverter());
			return factory;
		}
	
		@Override
		public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
			registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
		}

}
