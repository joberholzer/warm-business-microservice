package za.co.standardbank.interactivemessaging.model.response;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class JsonResponse 
{
    private String Message;
}