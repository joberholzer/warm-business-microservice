package za.co.standardbank.interactivemessaging.model.request;

import lombok.Data;

@Data
public class ReallocateConversation {

    public String reallocateType;

    public Long conversationId;

    public String username;
}
