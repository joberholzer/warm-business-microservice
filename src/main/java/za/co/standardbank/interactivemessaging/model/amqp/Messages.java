package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;

import lombok.*;

@Setter
@Getter
public class Messages implements Serializable
{
    private static final long serialVersionUID = 1L;
    public String channel;
    public String to;
    public String content;
    public String contentType;
    public String caption;
    public String clientMessageId;
    public String previewFirstUrl;
    public Hsm hsm;
}