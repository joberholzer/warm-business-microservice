package za.co.standardbank.interactivemessaging.model.entity;

import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import za.co.standardbank.interactivemessaging.model.Audit;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "role", schema = "warm")
public class Role extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="role_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long roleId;

    @Column(name="role_name")
    public String roleName;

    @OneToMany(
            mappedBy = "role",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public List<InternalUserRole> internalUsers = new ArrayList<>();
}