package za.co.standardbank.interactivemessaging.model.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class NewMessage {

    private Long conversationId;

    private String username;

    private String messageText;

    private LocalDateTime timestamp;
}
