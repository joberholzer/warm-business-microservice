package za.co.standardbank.interactivemessaging.model.entity;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.Audit;

import java.io.Serializable;
import javax.persistence.*;

@Data
@Entity
@Table(name = "cst", schema = "warm")
public class Cst extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="cst_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long cstId;

    @Column(name="cst_name")
    public String cstName;

}