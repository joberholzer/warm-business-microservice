package za.co.standardbank.interactivemessaging.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import za.co.standardbank.interactivemessaging.model.Audit;
import za.co.standardbank.interactivemessaging.model.enums.MessageStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Data
@Entity
@Table(name = "message", schema = "warm")
public class Message extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="msg_id")
    public String messageId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "conversation_id", referencedColumnName = "conversation_id")
    public Conversation conversation;

    @Column(name="msg_sender_id")
    public String senderId;

    @Column(name="msg_text")
    public String messageText;

    @Column(name="msg_status")
    public String messageStatus = "unread";

    @Column(name="msg_timestamp")
    public LocalDateTime messageTimestamp;
}
