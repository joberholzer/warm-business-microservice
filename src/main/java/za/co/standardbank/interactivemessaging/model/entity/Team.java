package za.co.standardbank.interactivemessaging.model.entity;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.Audit;

import java.io.Serializable;
import javax.persistence.*;

@Data
@Entity
@Table(name = "team", schema = "warm")
public class Team extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="team_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long teamId;

    @Column(name="team_prestige")
    public Boolean teamPrestige;
}