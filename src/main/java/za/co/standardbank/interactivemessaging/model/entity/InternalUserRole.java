package za.co.standardbank.interactivemessaging.model.entity;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.Audit;

import java.io.Serializable;

import javax.persistence.*;

@Data
@Entity
@Table(name = "internal_user_role", schema = "warm")
public class InternalUserRole extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="internal_user_role_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long userRoleAssignId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="username", referencedColumnName = "username")
    public InternalUser internalUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    public Role role;
}