package za.co.standardbank.interactivemessaging.model.dto_models;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.entity.Portfolio;
import za.co.standardbank.interactivemessaging.model.entity.Team;

@Data
public class DtoPortfolio {
    
    public Long portfolioId;

    public String username;

    public String name;

    public String surname;

    public Team team;

    public DtoPortfolio(Portfolio portfolio){
        
        this.portfolioId = portfolio.portfolioId;
        this.username = portfolio.rmInternalUser.username;
        this.name = portfolio.rmInternalUser.name;
        this.surname = portfolio.rmInternalUser.surname;
        this.team = portfolio.team;
    }
}