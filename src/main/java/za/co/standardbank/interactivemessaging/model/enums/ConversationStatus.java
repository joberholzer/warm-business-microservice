package za.co.standardbank.interactivemessaging.model.enums;


public enum ConversationStatus {
  NEW("NEW"),
  ACTIVE("ACTIVE"),
  ESCALATED("ESCALATED"),
  REFERRED("REFERRED"),
  CANCELED("CANCELED"),
  CLOSED("CLOSED"),
  UNASSIGNED("UNASSIGNED"),
  REALLOCATED("REALLOCATED"),
  TIMEOUT("TIMEOUT");

  public final String name;


  ConversationStatus(String s) {
    name = s;
  }

  public String getValue()
  {
    return this.name;
  }
}