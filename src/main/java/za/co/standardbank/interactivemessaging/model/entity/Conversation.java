package za.co.standardbank.interactivemessaging.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.Audit;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "conversation", schema = "warm")
public class Conversation extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="conversation_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long conversationId;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public Customer customer;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="username", referencedColumnName = "username")
    public InternalUser internalUser;

    @Column(name="conversation_status")
    public String conversationStatus;

    @Column(name="conversation_parent_id")
    public Long conversationParentId;

    @JsonManagedReference
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name="conversation_id", referencedColumnName = "conversation_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
    public List<Message> messages = new ArrayList<>();
    
}