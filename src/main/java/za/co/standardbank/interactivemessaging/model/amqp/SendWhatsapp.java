package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;
import java.util.List;

import lombok.*;

@Setter
@Getter
public class SendWhatsapp implements Serializable
{  
    private static final long serialVersionUID = 1L;
    public String IntegrationName;
    public String IntegrationId;   
    public List<Messages> Messages;
    public String EncryptionKey; 
    public String Sha256Hash;
}