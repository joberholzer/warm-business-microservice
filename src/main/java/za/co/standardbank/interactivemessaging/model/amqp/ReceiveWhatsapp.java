package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceiveWhatsapp implements Serializable
{
    private static final long serialVersionUID = 1L;
    public String IntegrationName;
    public String IntegrationId;
    public Event Event;
    private String Channel;
    private String MessageId;
    private String RelatedMessageId;
    private String RelatedClientMessageId;
    private String From;
    private String To;
    private String Timestamp;
    private String EncryptionKey;
    private String Charset;
    private String Content;
}