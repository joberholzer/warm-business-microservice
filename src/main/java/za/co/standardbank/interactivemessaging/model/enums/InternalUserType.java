package za.co.standardbank.interactivemessaging.model.enums;

public enum InternalUserType {
    TRANSACTIONAL_BANKER("TRANSACTIONAL_BANKER"),
    TEAM_LEAD("TEAM_LEAD"),
    RELATIONSHIP_MANAGER("RELATIONSHIP_MANAGER");

    public final String name;

    InternalUserType(String u) {
        name = u;
    }
    
    public String getValue()
    {
        return this.name;
    }
}