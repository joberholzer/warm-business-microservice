package za.co.standardbank.interactivemessaging.model.request;

import java.io.Serializable;

public class QualifyRequest implements Serializable {
    private String identityNumber;
    private String cellphoneNumber;

    public QualifyRequest(String identityNumber, String cellphoneNumber) {
        this.identityNumber = identityNumber;
        this.cellphoneNumber = cellphoneNumber;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getCellphoneNumber() {
        return cellphoneNumber;
    }

    public void setCellphoneNumber(String cellphoneNumber) {
        this.cellphoneNumber = cellphoneNumber;
    }

}
