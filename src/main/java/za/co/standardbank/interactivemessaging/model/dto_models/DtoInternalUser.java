package za.co.standardbank.interactivemessaging.model.dto_models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.Conventions;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.Cst;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoConversation;
import za.co.standardbank.interactivemessaging.model.entity.Team;
import za.co.standardbank.interactivemessaging.model.entity.UserType;

@Data
public class DtoInternalUser {

    public String username;

    public Cst cst;

    public UserType userType;

    public Team team;

    public String name;

    public String surname;

    public String password;

    public Boolean status;

    public List<DtoConversation> dtoConversations;

    public DtoInternalUser(InternalUser internalUser){

        this.username = internalUser.username;
        this.cst = internalUser.cst;
        this.userType = internalUser.userType;
        this.team = internalUser.team;
        this.name = internalUser.name;
        this.surname = internalUser.surname;
        this.password = internalUser.password;
        this.status = internalUser.status;
        this.dtoConversations = convert(internalUser.conversations);

    }

    public List<DtoConversation> convert(List<Conversation> c){

        List<DtoConversation> d = new ArrayList<>();

        if(c == null || c.isEmpty())
        {
            return null;
        }

        for (Conversation conversation : c) {
            d.add(new DtoConversation(conversation));            
        }

        return d;
    }

}
