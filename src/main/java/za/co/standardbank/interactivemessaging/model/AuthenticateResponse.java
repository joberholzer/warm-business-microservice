package za.co.standardbank.interactivemessaging.model;

import lombok.Data;

@Data
public class AuthenticateResponse {

    private String token;

    public AuthenticateResponse(String token) {
        this.token = token;
    }
}
