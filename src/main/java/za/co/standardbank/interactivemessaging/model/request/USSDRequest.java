package za.co.standardbank.interactivemessaging.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class USSDRequest implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private String identityNumber;
    private String cellphoneNumber;
    private boolean acceptTermsAndConditions;
    private boolean optInorOut;
}

