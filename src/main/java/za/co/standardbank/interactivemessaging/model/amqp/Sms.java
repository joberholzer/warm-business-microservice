package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Sms implements Serializable 
{
    private static final long serialVersionUID = 1L;
    public String udh;
    public String network;
}