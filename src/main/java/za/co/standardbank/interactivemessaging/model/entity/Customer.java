package za.co.standardbank.interactivemessaging.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import za.co.standardbank.interactivemessaging.model.Audit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Data
@Entity
@Table(name = "customer", schema = "warm")
public class Customer extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="cus_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long customerId;

    @OneToOne
    @JoinColumn(name = "portfolio_id", referencedColumnName = "portfolio_id")
    public Portfolio portfolio;

    @Column(name="cus_firstname")
    public String firstName;

    @Column(name="cus_middlename")
    public String middleName;

    @Column(name="cus_lastname")
    public String lastName;

    @Column(name="cus_mobile_number")
    public String mobileNumber;

    @JsonManagedReference
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name="conversation_id")
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
    public List<Conversation> conversations = new ArrayList<>();

    @Column(name = "cus_email")
    public String customerEmail;

    @Column(name = "cus_sa_id")
    public String customerSAId;

    @Column(name = "cus_tcs")
    public Boolean customerTCs;

    @Column(name = "cus_opt_status")
    public Boolean customerOptStatus;
}
