package za.co.standardbank.interactivemessaging.model.request;

import java.io.Serializable;

public class RegisterRequest implements Serializable {
    private String identityNumber;
    private String cellphoneNumber;
    private boolean acceptTermsAndConditions;

    public RegisterRequest(String identityNumber, String cellphoneNumber, boolean acceptTermsAndConditions) {
        this.identityNumber = identityNumber;
        this.cellphoneNumber = cellphoneNumber;
        this.acceptTermsAndConditions = acceptTermsAndConditions;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public String getCellphoneNumber() {
        return cellphoneNumber;
    }

    public boolean isAcceptTermsAndConditions() {
        return acceptTermsAndConditions;
    }

}
