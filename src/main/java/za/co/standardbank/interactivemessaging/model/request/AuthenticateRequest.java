package za.co.standardbank.interactivemessaging.model.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthenticateRequest implements Serializable {
    private String username;
    private String password;
}
