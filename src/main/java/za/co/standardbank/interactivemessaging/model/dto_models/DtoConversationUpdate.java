package za.co.standardbank.interactivemessaging.model.dto_models;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;

import java.util.ArrayList;
import java.util.List;

@Data
public class DtoConversationUpdate {

    public List<Conversation> oldConversations = new ArrayList<>();
    public List<Conversation> newConversations  = new ArrayList<>();

    public DtoConversationUpdate(List<Conversation> oldConversations, List<Conversation> newConversations) {

        this.oldConversations = oldConversations;
        this.newConversations = newConversations;
    }
}