package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoText implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Channel;
    public String MessageId;
    public String RelatedMessageId;
    public String RelatedClientMessageId;
    public String From;
    public String To;
    public String Timestamp;
    public String EncryptionKey;
    public String Charset;
    public String Content;
    public Sms Sms;

}