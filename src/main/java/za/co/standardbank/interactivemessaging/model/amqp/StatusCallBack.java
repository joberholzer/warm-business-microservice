package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;

import lombok.*;

@Setter
@Getter
public class StatusCallBack implements Serializable
{
    private static final long serialVersionUID = 1L;
    public String integrationId;
    public String integrationName;
    public String timestamp;
	public String statusCode;
    public String status;
    public String messageId;
    public String clientMessageId;
}