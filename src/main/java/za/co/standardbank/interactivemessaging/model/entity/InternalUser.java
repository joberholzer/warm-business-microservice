package za.co.standardbank.interactivemessaging.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import za.co.standardbank.interactivemessaging.model.Audit;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "internal_user", schema = "warm")
public class InternalUser extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="username")
    public String username;

    @OneToOne
    @JoinColumn(name="cst_id", referencedColumnName = "cst_id")
    public Cst cst;

    @OneToOne
    @JoinColumn(name="user_type_id", referencedColumnName = "user_type_id")
    public UserType userType;

    @ManyToOne
    @JoinColumn(name="team_id", referencedColumnName = "team_id")
    public Team team;

    @Column(name="usr_name")
    public String name;

    @Column(name="usr_surname")
    public String surname;

    @Column(name="usr_password")
    public String password;

    @Column(name="usr_status")
    public Boolean status;

    @JsonManagedReference
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name="username", referencedColumnName="username")
    public List<Conversation> conversations = new ArrayList<Conversation>();

}
