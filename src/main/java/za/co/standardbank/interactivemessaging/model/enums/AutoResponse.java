package za.co.standardbank.interactivemessaging.model.enums;

public enum AutoResponse {

    NOCUSTOMER("Dear valued customer, please note that this number is not registered to use this service. For assistance, please contact your relationship banker/ banking team on how to register for this service."),
    REG("Dear {firstName}, please note that this number is not registered to use this service. For assistance, please contact your relationship banker/ banking team on how to register for this service."),
    REFER("Hello {firstName}, we are attending to your request. Please come back in 48 hours to check on the progress."),
    FIRST_TIME("Dear {firstName}, welcome to Standard Bank WhatsApp service. Before continuing, please keep the following in mind: \n\nYou are using this service at your own risk, as per electronic instruction indemnity signed  \n\nAs a registered user, we have all your details on record and therefore there is no need to divulge any personal or account details when issuing instructions \n\nPlease maintain decorum and use clear, full sentences at all times, to enable speedy resolution of your queries/ instructions \n\nThank You!"),
    SECOND_TIME("Dear {firstName}, thank you for contacting Standard Bank, kindly let us know how we can assist. Our team of bankers will attend to your request and respond as soon as your query has been resolved or need more information from you. Thank you!"),
    CLOSED("Thank you for contacting us {firstName}. Anything else we can help you with, kindly let us know.");

    public final String name;


    AutoResponse(String s) {
        name = s;
    }

    public String getValue()
    {
        return this.name;
    }
}