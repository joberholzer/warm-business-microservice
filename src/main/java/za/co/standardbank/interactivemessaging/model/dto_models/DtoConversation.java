package za.co.standardbank.interactivemessaging.model.dto_models;

import java.util.List;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.Message;

@Data
public class DtoConversation {

    public Long conversationId;

    public DtoCustomer dtoCustomer;

    public Long customerId;

    public String customerFirstname;

    public String custometLastname;
    
    public DtoPortfolio dtoPortfolio;

    public String internalUserUsername;

    public String conversationStatus;

    public Long conversationParentId;

    public List<Message> messages;


    public DtoConversation(Conversation conversation) {

        this.conversationId = conversation.conversationId;
        this.customerId = conversation.customer.customerId;
        this.customerFirstname = conversation.customer.firstName;
        this.custometLastname = conversation.customer.lastName;
        this.dtoPortfolio = new DtoPortfolio(conversation.customer.portfolio);
        this.internalUserUsername = conversation.internalUser.username;
        this.conversationStatus = conversation.conversationStatus;
        this.conversationParentId = conversation.conversationParentId;
        this.messages = conversation.messages;
    }
}
