package za.co.standardbank.interactivemessaging.model.entity;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.Audit;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "portfolio", schema = "warm")
public class Portfolio extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="portfolio_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long portfolioId;

    @OneToOne
    @JoinColumn(name = "rm_username", referencedColumnName = "username", nullable=false, insertable=false, updatable=false)
    public InternalUser rmInternalUser;

    @ManyToOne
    @JoinColumn(name="team_id", referencedColumnName = "team_id")
    public Team team;

}