package za.co.standardbank.interactivemessaging.model.dto_models;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.entity.Customer;
import za.co.standardbank.interactivemessaging.model.entity.Portfolio;

@Data
public class DtoCustomer {

    public Long customerId;

    public DtoPortfolio dtoPortfolio;

    public String firstName;

    public String middleName;

    public String lastName;

    public String mobileNumber;

    public String customerSAId;

    public String customerEmail;


    public DtoCustomer(Customer customer) {

        this.customerId = customer.customerId;
        this.dtoPortfolio = new DtoPortfolio(customer.portfolio);
        this.firstName = customer.firstName;
        this.middleName = customer.middleName;
        this.lastName = customer.lastName;
        this.mobileNumber = customer.mobileNumber;
        this.customerSAId = customer.customerSAId;
        this.customerEmail = customer.customerEmail;
    }
}
