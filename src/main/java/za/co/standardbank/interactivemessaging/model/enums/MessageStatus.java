package za.co.standardbank.interactivemessaging.model.enums;

public enum MessageStatus {
    READ("read"),
    UNREAD("unread"),
    CANCELED("canceled");

    private final String name;

    MessageStatus(String m) {
        name = m;
    }

    public String getValue()
    {
        return this.name;
    }

}
