package za.co.standardbank.interactivemessaging.model.entity;

import lombok.Data;
import za.co.standardbank.interactivemessaging.model.Audit;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_type", schema = "warm")
public class UserType extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="user_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long userTypeId;

    @Column(name="user_type_name")
    public String userTypeName;
}