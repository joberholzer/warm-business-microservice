package za.co.standardbank.interactivemessaging.model.response;

import java.io.Serializable;

public class RegisterResponse implements Serializable {
    private int status;
    private String description;

    public RegisterResponse(int status, String description) {
        this.status = status;
        this.description = description;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
