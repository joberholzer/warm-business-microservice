package za.co.standardbank.interactivemessaging.model.amqp;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hsm implements Serializable{

	private static final long serialVersionUID = 1L;
	public String template;
    public String[] parameters;
}