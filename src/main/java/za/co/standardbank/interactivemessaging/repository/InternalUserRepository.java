package za.co.standardbank.interactivemessaging.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;

import java.util.List;

@Repository
public interface InternalUserRepository extends JpaRepository<InternalUser, Long> {

    InternalUser findByUsername(String username);

    List<InternalUser> findAllInternalUsersByUserType_userTypeNameAndTeam_TeamId(String userTypeName, Long teamId);

    List<InternalUser> findAllInternalUsersByStatusAndUserType_userTypeNameAndTeam_TeamId(Boolean status, String userTypeName, Long teamId);

    InternalUser findInternalUserByUserType_userTypeNameAndTeam_TeamId(String userTypeName, Long teamId);

    List<InternalUser> findAllInternalUsersByUserType_userTypeName(String userTypeName);
}