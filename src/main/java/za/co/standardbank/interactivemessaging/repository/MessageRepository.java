package za.co.standardbank.interactivemessaging.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import za.co.standardbank.interactivemessaging.model.entity.Message;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllMessagesBySenderId(String senderId);

    List<Message> findAllMessagesByConversation_ConversationId(Long conversationId);

    int countBySenderId(String senderId);

    int countBySenderIdAndConversation_ConversationId(String senderId, Long conversationId);
}