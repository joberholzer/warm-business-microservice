package za.co.standardbank.interactivemessaging.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import za.co.standardbank.interactivemessaging.model.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findCustomerByMobileNumber(String mobileNumber);

    List<Customer> findCustomersByConversations_ConversationStatus(String status);

    Customer findCustomersByCustomerId(Long customerId);

    List<Customer> findAllCustomersByPortfolio_PortfolioId(Long portfolioId);
}
