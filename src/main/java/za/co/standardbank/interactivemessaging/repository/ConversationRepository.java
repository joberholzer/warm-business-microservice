package za.co.standardbank.interactivemessaging.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Long> {

    Conversation findConversationByConversationId(Long conversationId);

    List<Conversation> findAllConversationsByCustomer_CustomerId(Long customerId);

    List<Conversation> findAllConversationsByCustomer_CustomerIdAndConversationStatus(Long customerId, String conversationStatus);

    List<Conversation> findAllConversationsByInternalUser_UsernameAndConversationStatusOrInternalUser_UsernameAndConversationStatus(String username1, String conversationstatus1, String username2, String conversationstatus2);
    
    List<Conversation> findConversationsByInternalUser_Username(String username);

    List<Conversation> findAllConversationsByConversationStatus(String conversationStatus);

    Conversation findConversationByCustomer_CustomerIdAndConversationStatus(Long customerId, String status);

    Conversation findConversationByCustomer_CustomerIdAndConversationStatusOrCustomer_CustomerIdAndConversationStatus(Long customerId1, String status1, Long customerId2, String status2);

    List<Conversation> findAllConversationByCustomer_CustomerIdAndConversationStatusOrCustomer_CustomerIdAndConversationStatus(Long customerId1, String status1, Long customerId2, String status2);

    List<Conversation> findAllConversationsByConversationStatusAndConversationParentIdNotNull(String conversationStatus);

    int countByCustomer_customerId(Long customerId);
}