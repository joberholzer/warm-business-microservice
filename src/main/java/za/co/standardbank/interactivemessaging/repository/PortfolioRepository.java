package za.co.standardbank.interactivemessaging.repository;

import org.springframework.stereotype.Repository;
import za.co.standardbank.interactivemessaging.model.entity.Portfolio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface PortfolioRepository extends JpaRepository<Portfolio, Long> {

    Portfolio findByPortfolioId(Long portfolioId);

    List<Portfolio> findAllPortfoliosByTeam_TeamId(Long teamId);   

    List<Portfolio> findAll();
}