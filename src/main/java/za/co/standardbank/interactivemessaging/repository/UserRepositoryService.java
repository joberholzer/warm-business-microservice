package za.co.standardbank.interactivemessaging.repository;

import org.springframework.stereotype.Repository;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import za.co.standardbank.interactivemessaging.model.request.UserDto;

@Repository
public interface UserRepositoryService extends JpaRepository<InternalUser, Long> {

    InternalUser findByUsername(String username);

}