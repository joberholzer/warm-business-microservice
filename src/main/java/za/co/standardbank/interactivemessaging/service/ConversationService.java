package za.co.standardbank.interactivemessaging.service;

import org.postgresql.translation.messages_bg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.standardbank.interactivemessaging.controller.MessageController;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.Customer;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.entity.Message;
import za.co.standardbank.interactivemessaging.repository.ConversationRepository;
import za.co.standardbank.interactivemessaging.repository.InternalUserRepository;
import za.co.standardbank.interactivemessaging.model.enums.ConversationStatus;
import za.co.standardbank.interactivemessaging.model.enums.InternalUserType;
import za.co.standardbank.interactivemessaging.model.enums.MessageStatus;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoConversation;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoConversationUpdate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import za.co.standardbank.interactivemessaging.repository.MessageRepository;


@Service("ConversationService")
public class ConversationService {

    private Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private ConversationRepository _conversationRepository;

    @Autowired
    private MessageRepository _messageRepository;

    @Autowired
    private MessageService _messageService;

    @Autowired
    private InternalUserRepository _internalUserRepository;

    public List<DtoConversation> convertConversations(List<Conversation> conversations) {
        List<DtoConversation> dtoConversions = conversations.stream().map(c ->
                new DtoConversation(c)).collect(Collectors.toList());

        return dtoConversions;
    }

    // Unused method, left for reference purpose
    public DtoConversationUpdate updateTbCloseConversations(List<Conversation> conversations) {
        
        List<Conversation> newConversations = new ArrayList<>();
        List<Conversation> oldConversations = new ArrayList<>();
        
        oldConversations = conversations.stream().map(conversation ->
                {
                    conversation.conversationStatus = ConversationStatus.CLOSED.getValue();
                    
                    newConversations.add(setConversation(conversation, ConversationStatus.NEW));

                    return conversation;
                }
        ).collect(Collectors.toList());

        return new DtoConversationUpdate(oldConversations, newConversations);
    }


    // Unused method, used for reference purposes
    public Conversation setConversation(Conversation oldConversation, ConversationStatus status) {

        Conversation newConversation = new Conversation();

        List<InternalUser> internalUsers = _internalUserRepository.findAllInternalUsersByStatusAndUserType_userTypeNameAndTeam_TeamId(true, InternalUserType.TRANSACTIONAL_BANKER.getValue(), oldConversation.internalUser.team.teamId);

        for (InternalUser internaluser : internalUsers) {

            internaluser.conversations = new ArrayList<>();

            List<Conversation> conversations = _conversationRepository.findAllConversationsByInternalUser_UsernameAndConversationStatusOrInternalUser_UsernameAndConversationStatus(
                internaluser.username, ConversationStatus.ACTIVE.getValue(), internaluser.username, ConversationStatus.NEW.getValue());

            internaluser.conversations.addAll(conversations);
        }

        InternalUser assignInternalUser = _messageService.getInternalUserCount(internalUsers);

        if(!assignInternalUser.equals(null)){     
            
            newConversation.internalUser = assignInternalUser;     
            newConversation.internalUser.conversations.addAll(assignInternalUser.conversations);    
        }        

        newConversation.customer = oldConversation.customer;
        newConversation.customer.conversations.addAll(oldConversation.customer.conversations);
        newConversation.conversationStatus = ConversationStatus.NEW.getValue();
        newConversation.conversationParentId = oldConversation.conversationId;
        newConversation.messages = new ArrayList<>();

        _conversationRepository.save(newConversation);

        return newConversation;
    }

    public Conversation escalateConversation(Long conversationId){

        Conversation conversation = new Conversation();
        conversation =  _conversationRepository.findConversationByConversationId(conversationId);

        Conversation newConversation = new Conversation();
        newConversation.customer = new Customer();
        newConversation.internalUser = new InternalUser();
        Conversation oldConversation = new Conversation();

        oldConversation = conversation;
        oldConversation.conversationStatus = ConversationStatus.ESCALATED.getValue();

        if(conversation.internalUser.userType.userTypeName.equals(InternalUserType.TRANSACTIONAL_BANKER.getValue())){

            newConversation.internalUser = _internalUserRepository.findInternalUserByUserType_userTypeNameAndTeam_TeamId(InternalUserType.TEAM_LEAD.getValue(), conversation.internalUser.team.teamId);

        }else if(conversation.internalUser.userType.userTypeName.equals(InternalUserType.TEAM_LEAD.getValue().toString())){

            newConversation.internalUser = _internalUserRepository.findInternalUserByUserType_userTypeNameAndTeam_TeamId(InternalUserType.RELATIONSHIP_MANAGER.getValue(), conversation.internalUser.team.teamId);
        }

        newConversation.customer.customerId = conversation.customer.customerId;
        newConversation.conversationStatus = ConversationStatus.NEW.getValue();
        newConversation.conversationParentId =  conversation.conversationId;

        _conversationRepository.save(oldConversation);
        _conversationRepository.save(newConversation);

        return newConversation;
    }

    public Conversation closeConversation(Long conversationId){

        Conversation conversation = new Conversation();

        conversation = _conversationRepository.findConversationByConversationId(conversationId);

        conversation.conversationStatus = ConversationStatus.CLOSED.getValue();

        _conversationRepository.save(conversation);
    
        return conversation;
    }

    public Conversation referConversation(Long conversationId){

        Conversation conversation = new Conversation();

        conversation = _conversationRepository.findConversationByConversationId(conversationId);

        conversation.conversationStatus = ConversationStatus.REFERRED.getValue();

        _conversationRepository.save(conversation);
    
        return conversation;
    }


    public Conversation reallocateAndOpenConversation(Conversation oldConversation, InternalUser internalUser, ConversationStatus status){

        Conversation newConversation = new Conversation();

        newConversation.conversationParentId = oldConversation.conversationId;
        newConversation.customer = oldConversation.customer;
        newConversation.customer.conversations.addAll(oldConversation.customer.conversations);
        newConversation.internalUser = internalUser;
        newConversation.conversationStatus = ConversationStatus.NEW.getValue();

        oldConversation.conversationStatus = ConversationStatus.REALLOCATED.getValue();

        _conversationRepository.save(oldConversation);
        _conversationRepository.save(newConversation);

        return newConversation;
    }

    public List<Conversation> getAllEscalatedConversations(){
        
        List<Conversation> conversationsNew = new ArrayList<>();
        List<Conversation> conversationsEscalated = new ArrayList<>();
        List<Conversation> conversations = new ArrayList<>();

        conversationsNew = _conversationRepository.findAllConversationsByConversationStatusAndConversationParentIdNotNull(ConversationStatus.NEW.getValue());

        if(conversationsNew == null || conversationsNew.isEmpty()){
            return null;
        }

        conversationsEscalated = _conversationRepository.findAllConversationsByConversationStatus(ConversationStatus.ESCALATED.getValue());

        if(conversationsEscalated == null || conversationsEscalated.isEmpty()){
            return null;
        }
        
        for (Conversation conversationNew : conversationsNew) {
            for (Conversation conversationEscalated : conversationsEscalated) {
                if(conversationNew.conversationParentId == conversationEscalated.conversationId){
                    conversations.add(conversationNew);
                }
            }
        }
        
        return conversations;
    }

    List<Message> msgs = new ArrayList<>();
    List<Message> messages = new ArrayList<>();

    public List<Conversation> getActiveConversationForInternalUser(String username){

        List<Conversation> conversations =  _conversationRepository.findAllConversationsByInternalUser_UsernameAndConversationStatusOrInternalUser_UsernameAndConversationStatus(username, ConversationStatus.ACTIVE.getValue(), username, ConversationStatus.NEW.getValue());

        for (Conversation conversation : conversations) {

            msgs = new ArrayList<>(); 
            msgs = initRecursion(conversation);

            for(Message msg : msgs){
                conversation.messages.add(msg);
            }
        }

        return conversations;
    }

    public List<Message> initRecursion(Conversation conversation){
       
        messages = new ArrayList<>();

        messages = GetParentMessages(conversation);

        return messages;
    }

    public List<Message> GetParentMessages(Conversation conversation){

        if(conversation.conversationParentId != null){

            Conversation oldConversation = _conversationRepository.findConversationByConversationId(conversation.conversationParentId);

            for (Message message : oldConversation.messages) {
                
                if(message != null)
                {
                    messages.add(message);
                }
            }

            GetParentMessages(oldConversation);
        }

        return messages;
    }

    public void SetAllMessagestoReadforConversation(Long conversationId)
    {
        List<Message> msg = new ArrayList<>();
        Conversation conversation = new Conversation();

        msg = _messageRepository.findAllMessagesByConversation_ConversationId(conversationId);
        conversation = _conversationRepository.findConversationByConversationId(conversationId);

        for(Message message : msg)
        {
            message.messageStatus = MessageStatus.READ.getValue();
            _messageRepository.save(message);
        }

        conversation.conversationStatus = ConversationStatus.ACTIVE.getValue();
        _conversationRepository.save(conversation);

    }
}
