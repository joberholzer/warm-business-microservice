package za.co.standardbank.interactivemessaging.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import za.co.standardbank.interactivemessaging.controller.MessageController;
import za.co.standardbank.interactivemessaging.model.entity.Portfolio;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.repository.InternalUserRepository;
import za.co.standardbank.interactivemessaging.repository.PortfolioRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service("PortfolioService")
public class PortfolioService {

    @Autowired
    MessageService messageService;
    @Autowired
    private PortfolioRepository _portfolioRepository;
    @Autowired
    private InternalUserRepository _internalUserRepository;

    private Logger logger = LoggerFactory.getLogger(MessageController.class);

    public List<Portfolio> getPortfolioByTb(String username){

        InternalUser internalUser = _internalUserRepository.findByUsername(username);

        List<Portfolio> portfolios = _portfolioRepository.findAllPortfoliosByTeam_TeamId(internalUser.team.teamId);

        return portfolios;
    }
}
