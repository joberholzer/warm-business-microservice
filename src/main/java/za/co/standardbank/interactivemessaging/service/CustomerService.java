package za.co.standardbank.interactivemessaging.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import za.co.standardbank.interactivemessaging.controller.MessageController;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoConversation;
import za.co.standardbank.interactivemessaging.model.dto_models.DtoCustomer;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.Customer;
import za.co.standardbank.interactivemessaging.model.request.USSDRequest;
import za.co.standardbank.interactivemessaging.repository.CustomerRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service("CustomerService")
public class CustomerService {

    @Autowired
    private CustomerRepository _customerRepository;

    private Logger logger = LoggerFactory.getLogger(MessageController.class);

    public List<DtoConversation> convertConversations(List<Conversation> conversations) {
        List<DtoConversation> dtoConversions = conversations.stream().map(c ->
                new DtoConversation(c)).collect(Collectors.toList());

        return dtoConversions;
    }

    public DtoCustomer convertCustomer(Customer customer) {
        DtoCustomer dtoCustomer = new DtoCustomer(customer);

        return dtoCustomer;
    }

    public Customer registerCustomer(USSDRequest request) 
    {
        logger.info("opt-in or out from whatsapp service...");
        Customer customer = _customerRepository.findCustomerByMobileNumber(request.getCellphoneNumber());

        try 
        {
            customer = updateCustomerInfo(request, customer);
        } 
        catch (UsernameNotFoundException ex) 
        {
           logger.error(ex.getMessage());

           throw new UsernameNotFoundException(ex.getMessage());
        }

        return customer;
    }

    public Boolean checkCustomerQualify(USSDRequest request) 
    {
        logger.info("check if customer qualify for whatsapp service...");

        Customer customer = _customerRepository.findCustomerByMobileNumber(request.getCellphoneNumber());

        if(customer == null)
        {
            throw new UsernameNotFoundException("Unable to find customer " + HttpStatus.NOT_FOUND);
        }

        if (!customer.getCustomerSAId().equals(request.getIdentityNumber())) 
        {
            return false;
        }

        return true;
    }

    private Customer updateCustomerInfo(USSDRequest request, Customer customer) 
    {
        logger.info("update customer information...");

        if (customer == null && !request.isAcceptTermsAndConditions()) 
        {
            throw new UsernameNotFoundException("Unable to find customer " + HttpStatus.NOT_FOUND);
        }
            customer.setCustomerId(customer.getCustomerId());
            customer.setCustomerOptStatus(request.isOptInorOut());
            customer.setCustomerTCs(request.isAcceptTermsAndConditions());

            //Update the customer
            Customer updatedCustomer = _customerRepository.save(customer);

        return updatedCustomer;
    }
}
