package za.co.standardbank.interactivemessaging.service;

import org.springframework.stereotype.Service;
import za.co.standardbank.interactivemessaging.controller.AuthenticationController;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.request.UserDto;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import za.co.standardbank.interactivemessaging.repository.UserRepositoryService;

import java.util.ArrayList;

@Service("JwtUserDetailsService")
public class JwtUserDetailsService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    private UserRepositoryService userRepositoryService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        InternalUser user = userRepositoryService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }

    public InternalUser register(UserDto user) {
        InternalUser newUser = new InternalUser();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        logger.info("...about to save user to the database: "+ user.toString());
        return userRepositoryService.save(newUser);
    }
}
