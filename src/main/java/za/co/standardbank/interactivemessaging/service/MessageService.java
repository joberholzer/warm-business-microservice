package za.co.standardbank.interactivemessaging.service;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.*;

import za.co.standardbank.interactivemessaging.config.ApplicationConfigReader;
import za.co.standardbank.interactivemessaging.controller.MessageController;
import za.co.standardbank.interactivemessaging.model.amqp.Messages;
import za.co.standardbank.interactivemessaging.model.amqp.MoText;
import za.co.standardbank.interactivemessaging.model.amqp.ReceiveWhatsapp;
import za.co.standardbank.interactivemessaging.model.amqp.SendWhatsapp;
import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.model.entity.Customer;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.model.entity.Message;
import za.co.standardbank.interactivemessaging.model.entity.Portfolio;
import za.co.standardbank.interactivemessaging.model.enums.AutoResponse;
import za.co.standardbank.interactivemessaging.model.enums.ConversationStatus;
import za.co.standardbank.interactivemessaging.model.enums.InternalUserType;
import za.co.standardbank.interactivemessaging.model.enums.MessageStatus;
import za.co.standardbank.interactivemessaging.model.request.NewMessage;
import za.co.standardbank.interactivemessaging.rabbitmq.MessageSender;
import za.co.standardbank.interactivemessaging.repository.ConversationRepository;
import za.co.standardbank.interactivemessaging.repository.CustomerRepository;
import za.co.standardbank.interactivemessaging.repository.InternalUserRepository;
import za.co.standardbank.interactivemessaging.repository.MessageRepository;
import za.co.standardbank.interactivemessaging.repository.PortfolioRepository;

import java.io.FileReader;
import java.lang.reflect.Array;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service("MessageService")
public class MessageService {

    private Logger logger = LoggerFactory.getLogger(MessageService.class);

    @Autowired
    CustomerRepository _customerRepository;

    @Autowired
    ConversationRepository _conversationRepository;

    @Autowired
    MessageRepository _messageRepository;

    @Autowired
    private MessageSender _messageSender;

    @Autowired
    private RabbitTemplate _rabbitTemplate;

    @Autowired
    private ApplicationConfigReader applicationConfigReader;

    @Autowired
    PortfolioRepository _portfolioRepository;

    @Autowired
    InternalUserRepository _internalUserRepository;

    public Message manageIncomingMessage (ReceiveWhatsapp reqObj) {

        // Get telephone number in db format
        String number = reqObj.getFrom();

        try{
            
        Customer customer = _customerRepository.findCustomerByMobileNumber(number);

        if(customer == null){

            Conversation tempConversation = new Conversation();

            tempConversation.customer = new Customer();
            tempConversation.customer.mobileNumber = number;

            sendAutomatedMessage(tempConversation, AutoResponse.NOCUSTOMER);
        }

        if(customer.customerOptStatus.equals(false)){
            Conversation tempConversation = new Conversation();

            tempConversation.customer = customer;

            sendAutomatedMessage(tempConversation, AutoResponse.REG);
        }

        int conversationCount = 0;
        conversationCount = _conversationRepository.countByCustomer_customerId(customer.customerId);

        // Find new or active conversation related user (max of 1 should be available)
        Conversation conversation = _conversationRepository.findConversationByCustomer_CustomerIdAndConversationStatusOrCustomer_CustomerIdAndConversationStatus(
                customer.customerId, ConversationStatus.NEW.getValue(), customer.customerId, ConversationStatus.ACTIVE.getValue());

        // If no conversation found, create new one
        if (conversation == null){

            conversation = createConversation(customer);
        }

        // Find number of messages for customer
        int msgCount =  0;

        msgCount = _messageRepository.countBySenderIdAndConversation_ConversationId(customer.customerId.toString(), conversation.conversationId);


        // Verify auto response type
        AutoResponse responseType = getAutoResponseType(customer, null, msgCount, conversationCount);

        Message mappedMsg = mapIncomingMessage(reqObj, customer, conversation);       

        if(!conversation.conversationStatus.equals(ConversationStatus.ACTIVE.getValue()) && msgCount == 0)
        {
            sendAutomatedMessage(conversation, responseType);
        }

        _messageRepository.save(mappedMsg);

        return mappedMsg;

        } catch (Exception e) {
           logger.error("Following exception occured", e);
        }
       
        return null;
    }


    public void sendAutomatedMessage(Conversation conversation, AutoResponse responseType) {
        
        try {

            if(responseType.equals(AutoResponse.NOCUSTOMER)){

                // Create db Message record
                mapAutoMessage(conversation, responseType.getValue());

                mapWhatsappMessage(responseType.getValue(), conversation);
            }
            else if(responseType.equals(AutoResponse.REG)){
                
                String responseMsg = responseType.getValue().replace("{firstName}", conversation.customer.firstName);
                
                mapAutoMessage(conversation, responseMsg);
                
                mapWhatsappMessage(responseType.getValue(), conversation);
            }
            else{

                String responseMsg = responseType.getValue().replace("{firstName}", conversation.customer.firstName);
                // Create db Message record
                Message autoMessage = mapAutoMessage(conversation, responseMsg);
    
                // Create message object to send to messaging queue
                _messageRepository.save(autoMessage);
                mapWhatsappMessage(responseMsg, conversation);
            }

        } catch (Exception e) {
            logger.error("Following exception occured", e);
        }

    }

    private AutoResponse getAutoResponseType(Customer customer, ConversationStatus oldConversationStatus, int count, int conversationCount) {

        if (!customer.customerOptStatus)
            return AutoResponse.REG;

        if (count == 0 && conversationCount == 0)
            return AutoResponse.FIRST_TIME;

        if (count == 0 && conversationCount > 0)
            return AutoResponse.SECOND_TIME;

        try {
            if (oldConversationStatus == ConversationStatus.REFERRED)
                return AutoResponse.REFER;

            else if (oldConversationStatus == ConversationStatus.CLOSED)
                return AutoResponse.CLOSED;

        } catch (Exception e) {
            logger.error("Following exception occured", e);
        }

        return null;
    }

    private static LocalDateTime millsToLocalDateTime(String millis) {
        Instant instant = Instant.ofEpochMilli(Long.parseLong(millis));
        LocalDateTime date = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
        return date;
    }

    public Conversation createConversation(Customer customer) {

        try{

            Conversation conversation = new Conversation();

            Portfolio portfolio = _portfolioRepository.findByPortfolioId(customer.portfolio.portfolioId);
            
            List<InternalUser> internalUsers = _internalUserRepository.findAllInternalUsersByStatusAndUserType_userTypeNameAndTeam_TeamId(true ,InternalUserType.TRANSACTIONAL_BANKER.getValue(), portfolio.team.teamId);

            for (InternalUser internaluser : internalUsers) {

                internaluser.conversations = new ArrayList<>();

                List<Conversation> conversations = _conversationRepository.findAllConversationsByInternalUser_UsernameAndConversationStatusOrInternalUser_UsernameAndConversationStatus(
                    internaluser.username, ConversationStatus.ACTIVE.getValue(), internaluser.username, ConversationStatus.NEW.getValue());

                internaluser.conversations.addAll(conversations);
            }

            InternalUser internalUser = getInternalUserCount(internalUsers);

            if(!internalUser.equals(null)){            
                conversation.internalUser = internalUser;            
            }

            conversation.customer = customer;
            conversation.conversationStatus = ConversationStatus.NEW.getValue();

            _conversationRepository.save(conversation);

            return conversation;
        
        }catch(Exception e){
        
            logger.error("Following exception occured", e);
            return null;
        }        
    }


    private Message mapIncomingMessage(ReceiveWhatsapp reqObj, Customer customer, Conversation conversation) {

        MoText mo = reqObj.Event.moText.get(0);

        Message msg = new Message();

        msg.setMessageId(UUID.randomUUID().toString());
        msg.setMessageText(mo.Content);
        msg.setConversation(conversation);
        msg.setSenderId(customer.customerId.toString());
        msg.setMessageStatus(MessageStatus.UNREAD.getValue());
        msg.setMessageTimestamp(millsToLocalDateTime(mo.Timestamp));

        return msg;
    }


    private Message mapAutoMessage(Conversation newConversation, String response) {

        Message autoMessage = new Message();

        autoMessage.setMessageId(UUID.randomUUID().toString());
        autoMessage.setConversation(newConversation);
        autoMessage.setMessageTimestamp(LocalDateTime.now());
        autoMessage.setMessageText(response);
        autoMessage.setMessageStatus(MessageStatus.UNREAD.getValue());
        autoMessage.setSenderId("automated");

        return autoMessage;
    }


    public void mapWhatsappMessage(String response, Conversation conversation) {

        Messages msg = new Messages();

        msg.setContent(response);
        msg.setTo(conversation.customer.mobileNumber);
        msg.setChannel("whatsapp");

        SendWhatsapp send = new SendWhatsapp();
        send.setIntegrationId(applicationConfigReader.getToWhatsappMsgIntegrationId());
        send.setIntegrationName(applicationConfigReader.getToWhatsappMsgIntegrationName());

        send.Messages = new ArrayList<>();
        send.Messages.add(msg);

        _messageSender.sendMessage(_rabbitTemplate, applicationConfigReader.getToWhatsappExchange(), applicationConfigReader.getToWhatsappRoutingKey(), send);

        if(!conversation.conversationId.equals(null)){
            _conversationRepository.save(conversation);
        }
    }


    public Message postNewMessage(NewMessage newMessage, Conversation conversation) {

        Message msg = new Message();

        msg.setMessageId(UUID.randomUUID().toString());
        msg.setConversation(conversation);
        msg.setSenderId(newMessage.getUsername());
        msg.setMessageText(newMessage.getMessageText());
        msg.setMessageTimestamp(LocalDateTime.now());

        return msg;
    }

    public InternalUser getInternalUserCount(List<InternalUser> internalUsers){

        InternalUser internalUserReturn = new InternalUser();
        int minCount = internalUsers.get(0).conversations.size();

        for (InternalUser internalUser : internalUsers) {
            
            if(internalUser.conversations.size() <= minCount){

                minCount = internalUser.conversations.size();
                
                internalUserReturn = internalUser;
            }
        }

        return internalUserReturn;  
    }

}