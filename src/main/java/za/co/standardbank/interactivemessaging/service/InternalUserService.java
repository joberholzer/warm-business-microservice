package za.co.standardbank.interactivemessaging.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.standardbank.interactivemessaging.controller.MessageController;
import za.co.standardbank.interactivemessaging.model.entity.InternalUser;
import za.co.standardbank.interactivemessaging.repository.InternalUserRepository;

@Service("InternalUserService")
public class InternalUserService {

    private Logger logger = LoggerFactory.getLogger(MessageController.class);
    
    @Autowired
    private InternalUserRepository _internalUserRepository;

    public InternalUser deactivateUser(InternalUser internalUser) {

        internalUser.status = false;
        return internalUser;
    }

    public InternalUser deactivateTransBanker(String username){
        
        InternalUser internalUser = new InternalUser();

        internalUser = _internalUserRepository.findByUsername(username);

        internalUser.status = false;
        _internalUserRepository.save(internalUser);

        return internalUser;
    }

    public InternalUser activateTransBanker(String username){
        
        InternalUser internalUser = new InternalUser();

        internalUser = _internalUserRepository.findByUsername(username);

        internalUser.status = true;
        _internalUserRepository.save(internalUser);

        return internalUser;
    }
}