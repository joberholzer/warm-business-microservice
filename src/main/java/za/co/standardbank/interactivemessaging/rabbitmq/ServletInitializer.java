package za.co.standardbank.interactivemessaging.rabbitmq;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import za.co.standardbank.interactivemessaging.CoreAuthServiceApplication;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CoreAuthServiceApplication.class);
	}

}

