package za.co.standardbank.interactivemessaging.rabbitmq;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import za.co.standardbank.interactivemessaging.helper.ConstantFile.ApplicationConstant;
import za.co.standardbank.interactivemessaging.model.amqp.ReceiveWhatsapp;
import za.co.standardbank.interactivemessaging.model.entity.Message;
import za.co.standardbank.interactivemessaging.service.MessageService;


@Service
public class MessageListener {

    private static final Logger log = LoggerFactory.getLogger(MessageListener.class);

    @Autowired
    MessageService _messageService;


    @RabbitListener(queues = "${fromWhatsapp.queue.name}")
    public void receiveMessageToWhatsapp(ReceiveWhatsapp reqObj) 
    {
    	log.info("Received message: {} from fromWhatsapp queue.", reqObj);

        try 
        {
            log.info("Write message into DB");

            Message msg = _messageService.manageIncomingMessage(reqObj);

            log.info("Wrote message " + msg.getMessageId() + " to db.");

        } catch(HttpClientErrorException  ex) 
        {	
            if(ex.getStatusCode() == HttpStatus.NOT_FOUND) 
            {
        		log.info("Delay...");
                try 
                {
    				Thread.sleep(ApplicationConstant.MESSAGE_RETRY_DELAY);
                } catch (InterruptedException e) 
                { 

                }
    			
    			log.info("Throwing exception so that message will be requed in the queue.");
    			// Note: Typically Application specific exception can be thrown below
    			throw new RuntimeException();
    		} else {
    			throw new AmqpRejectAndDontRequeueException(ex); 
    		}
    		
        } catch(Exception e) 
        {
    		log.error("Internal server error occurred in python server. Bypassing message requeue {}", e);
    		throw new AmqpRejectAndDontRequeueException(e); 
    	}

    }
}
