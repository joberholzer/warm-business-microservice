package za.co.standardbank.interactivemessaging.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Optional;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    @Value("${jwt.secret}")
    private String secret;
    private static final String PREFFERED_USERNAME = "onpremisessamaccountname";
    private static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAni9SAyu9EsltQlV7Jo3w\n" +
            "MUvcpYb4mmfHzV4IsDZ6NQvJjtQJuhsfqiG86VntMd76R44kCmkfMGvtQRA2/Umn\n" +
            "VBSSLxQKvcGUqNodH7YaMYOTmHlbOSoVpi3Ox2wj6cWvhaTTm/4xzJ3F0yF0Y/aR\n" +
            "BMxSCIwLv3nTMRNe74k4zdBnhL7k5ObOY/vUGt/5+sPo6BXoV7oov4Ps6jeyUdRK\n" +
            "tqVZSp5/kzz16kPh1Ng/2tn4vpQimNbHRralq8rNM/gOLPAar6v7mL/qsqpgx+48\n" +
            "e5ENFxikbB+NzAmLll1QSkzciu2rCjFGH4j/+bCHr7FxUNDL/E0vMFVDFw8SUlYM\n" +
            "gQIDAQAB";

    //retrieve email from jwt token
    public String getUsernameFromToken(String token) {
        return getAllClaimsFromToken(token).get(PREFFERED_USERNAME).toString();
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieving any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(getParsedPublicKey().get()).parseClaimsJws(token).getBody();
        return claims;
    }

    //check if the token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    //validate token
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public static Optional<RSAPublicKey> getParsedPublicKey() {

        try {
            byte[] decode = Base64.decodeBase64(PUBLIC_KEY);
            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(decode);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(keySpecX509);
            return Optional.of(pubKey);

        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.out.println("Exception block | Public key parsing error ");
            return Optional.empty();
        }
    }
}
