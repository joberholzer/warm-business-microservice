package za.co.standardbank.interactivemessaging.helper;

import com.google.gson.Gson;

public class JsonConverter {

    public String JsonConvert(Object j){
        Gson gson = new Gson();
        String json = gson.toJson(j).toLowerCase();
      
        return json;
      }
}