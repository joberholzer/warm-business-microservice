package za.co.standardbank.interactivemessaging.helper;

import org.springframework.stereotype.Component;


public class ConstantFile
{
    @Component 
    public class ApplicationConstant {

        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";
    
        public static final String IN_PROGRESS = "INPROGRESS";
        public static final String IN_QUEUE = "REQUEST_IN_QUEUE";
        public static final String FAILED = "FAILED";
    
        /* Error codes Starts */
        public static final String MESSAGE_QUEUE_SEND_ERROR = "MESSAGE_QUEUE_SEND_ERROR";
        public static final String MESSAGE_QUEUE_RECEIVE_ERROR = "MESSAGE_QUEUE_RECEIVE_ERROR";
        
        /* Constants */
        public static final int MESSAGE_RETRY_DELAY=5000;

        /* Standard Error responces*/
        public static final String NoContent ="Object does not have any content.";
        public static final String CanNotBeNull ="Variable cannot be null.";
        public static final String NotFound = "Object cannot be found.";
        public static final String Forbidden = "Customer is not allowed to access the function.";

        public static final String CustomerAlreadyRegistered = "Customer has already been registered.";
        public static final String CustomerAlreadyDeregistered = "Customer has already been deregistered.";

        public static final String CustomerRegisteredSuccessfully = "Customer has been registered successfully.";
        public static final String CustomerDeregisteredSuccessfully = "Customer has been deregistered successfully.";
        
               
    }
}