package za.co.standardbank.interactivemessaging.helper;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import za.co.standardbank.interactivemessaging.model.entity.Conversation;
import za.co.standardbank.interactivemessaging.repository.ConversationRepository;
import za.co.standardbank.interactivemessaging.service.ConversationService;

@Component
public class ScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
    
    @Autowired
    private ConversationRepository _conversationRepository;
    @Autowired
    private ConversationService _conversationService;
    
    @Scheduled(fixedRate = 3600000)
    public void EscalateConversationsBasedOnTime()
    {
        log.info("Escalate all conversations that is new after 12 hours.This Runs Every Hour.");
        List<Conversation> conversationList = new ArrayList<>(); ;
        conversationList =  _conversationRepository.findAllConversationsByConversationStatus("NEW");
       
        final Date localDateTime = new Date();
        final long HOUR = 3600000 * 12; // in milli-seconds.

        if(!conversationList.isEmpty() || !conversationList.equals(null)){

            for (final Conversation conversation : conversationList) 
            {
                final Date newDate = new Date(conversation.getCreatedAt().getTime() + HOUR);
    
                if(newDate.before(localDateTime))
                {
                    _conversationService.escalateConversation(conversation.conversationId);
                    //todo: send Email to the escalated person
                }
            }
        }
    }

    @Scheduled(fixedRate = 3600000)
    public void CloseConversationsOlderThan24hours()
    {
        log.info("Close all conversations Older than 24 Hours.This Runs Every Hour.");
        List<Conversation> conversationList = new ArrayList<>(); ;
        conversationList =  _conversationRepository.findAllConversationsByConversationStatus("ACTIVE");

        final Date localDateTime = new Date();
        final long HOUR = 3600000 * 24; // in milli-seconds.
        
        if(!conversationList.isEmpty() || !conversationList.equals(null)){
            
            for (final Conversation conversation : conversationList) 
            {
                final Date newDate = new Date(conversation.getUpdatedAt().getTime() + HOUR);
    
                if(newDate.before(localDateTime))
                {
                    _conversationService.closeConversation(conversation.conversationId);
                }
            }
        }
    }
}