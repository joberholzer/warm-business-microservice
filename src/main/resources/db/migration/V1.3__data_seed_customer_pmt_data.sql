INSERT INTO warm.customer (cus_id, portfolio_id, cus_mobile_number, created_at, updated_at, cus_sa_id, cus_tcs, cus_opt_status, cus_firstname, cus_middlename, cus_lastname)
VALUES 
	(202020, 54321, '27740037636', CURRENT_TIMESTAMP, null, 'XCRE23', true, true, 'pmt1', null, 'surname'),
    (212121, 54321, '27732019123', CURRENT_TIMESTAMP, null, 'H43555RRE', true, true, 'pmt2', null, 'surname'),
    (222222, 54321, '27829806421', CURRENT_TIMESTAMP, null, 'DC586969', true, true, 'pmt3', null, 'surname'),
    (232323, 54321, '27814135130', CURRENT_TIMESTAMP, null, 'DR2AQ1D', true, true, 'pmt4', null, 'surname'),
    (242424, 54321, '27782017123', CURRENT_TIMESTAMP, null, 5407061075091, true, true, 'pmt5', null, 'surname'),
    (252525, 54321, '27632018123', CURRENT_TIMESTAMP, null, 7707102431098, true, true, 'pmt6', null, 'surname'),
    (262626, 54321, '27832016123', CURRENT_TIMESTAMP, null, 5208104687086, true, true, 'pmt7', null, 'surname'),
    (272727, 54321, '27825762443', CURRENT_TIMESTAMP, null, 5412166804098, true, true, 'pmt8', null, 'surname')
    ON CONFLICT (cus_id) DO NOTHING;