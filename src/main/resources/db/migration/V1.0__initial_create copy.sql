CREATE TABLE IF NOT EXISTS warm.flyway_schema_history 
(
  installed_rank INT NOT NULL,
  version VARCHAR(50),
  description VARCHAR(200) NOT NULL,
  type VARCHAR(20) NOT NULL,
  script VARCHAR(1000) NOT NULL,
  checksum INTEGER,
  installed_by VARCHAR(100) NOT NULL,
  installed_on TIMESTAMP NOT NULL DEFAULT now(),
  execution_time INTEGER NOT NULL,
  success BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS warm.user_type
(
  user_type_id BIGSERIAL PRIMARY KEY,
  user_type_name VARCHAR(50),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.team
(
  team_id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.cst
(
  cst_id BIGSERIAL PRIMARY KEY,
  cst_name VARCHAR(50),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.internal_user
(
  username VARCHAR(30) PRIMARY KEY,
  cst_id BIGINT REFERENCES cst (cst_id),
  user_type_id BIGINT REFERENCES user_type (user_type_id),
  team_id BIGINT REFERENCES team (team_id),
  usr_name VARCHAR(30),
  usr_surname VARCHAR(30),
  usr_password VARCHAR(50),
  usr_status BOOLEAN,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.portfolio
(
  portfolio_id BIGSERIAL PRIMARY KEY,
  rm_username VARCHAR(50) REFERENCES internal_user (username),
  team_id BIGINT REFERENCES team (team_id),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.customer
(
  cus_id BIGSERIAL PRIMARY KEY,
  portfolio_id BIGINT REFERENCES portfolio (portfolio_id),
  cus_mobile_number VARCHAR(20),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  cus_sa_id VARCHAR(20),
  cus_tcs BOOLEAN,
  cus_opt_status BOOLEAN,
  cus_firstname VARCHAR(30),
  cus_middlename VARCHAR(30),
  cus_lastname VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS warm.role
(
  role_id BIGSERIAL PRIMARY KEY,
  role_name VARCHAR(50),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


CREATE TABLE IF NOT EXISTS warm.internal_user_role
(
  internal_user_role_id BIGSERIAL PRIMARY KEY,
  username VARCHAR(30) REFERENCES internal_user (username),
  role_id BIGINT REFERENCES role (role_id),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.conversation
(
  conversation_id BIGSERIAL PRIMARY KEY,
  cus_id BIGINT REFERENCES customer (cus_id),
  username VARCHAR(30) REFERENCES internal_user (username),
  conversation_status VARCHAR(30),
  conversation_parent_id BIGINT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS warm.message
(
  msg_id VARCHAR(50) PRIMARY KEY,
  conversation_id BIGINT REFERENCES conversation (conversation_id),
  msg_sender_id VARCHAR(30),
  msg_text VARCHAR,
  msg_status VARCHAR(20),
  msg_timestamp TIMESTAMP,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);