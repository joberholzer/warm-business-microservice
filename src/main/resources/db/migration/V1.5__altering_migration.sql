delete from warm.customer where cus_id = 3;

INSERT INTO warm.customer (cus_id, portfolio_id, cus_mobile_number, created_at, updated_at, cus_sa_id, cus_tcs, cus_opt_status, cus_firstname, cus_middlename, cus_lastname)
VALUES 
	(3, 2, '27631733668', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Tlotlo', null, 'Rakgahla')
    ON CONFLICT (cus_id) DO NOTHING;