INSERT INTO warm.user_type (user_type_id, user_type_name, created_at) 
VALUES 
	(700, 'TRANSACTIONAL_BANKER', CURRENT_TIMESTAMP), 
	(701, 'TEAM_LEAD', CURRENT_TIMESTAMP),
	(702, 'RELATIONSHIP_MANAGER', CURRENT_TIMESTAMP) ON CONFLICT (user_type_id) DO NOTHING;

INSERT INTO warm.role (role_id, role_name, created_at) 
VALUES 
	(001, 'ADMIN', CURRENT_TIMESTAMP), 
	(002, 'SYSTEM_USER', CURRENT_TIMESTAMP) ON CONFLICT (role_id) DO NOTHING;

INSERT INTO warm.cst (cst_id, cst_name, created_at) 
VALUES 
	(1111, 'Johannesburg', CURRENT_TIMESTAMP), (2222, 'Pretoria', CURRENT_TIMESTAMP) ON CONFLICT (cst_id) DO NOTHING;

INSERT INTO warm.team (team_id, created_at) 
VALUES 
	(30000, CURRENT_TIMESTAMP), 
	(50000, CURRENT_TIMESTAMP) ON CONFLICT (team_id) DO NOTHING;

INSERT INTO warm.internal_user (username, cst_id, team_id, user_type_id, created_at, usr_name, usr_surname, usr_password, usr_status)
VALUES 
	('TransactionalBanker 1', 1111, 30000, 700, CURRENT_TIMESTAMP, 'TB 1', 'surname', 'ofnf34ityhfoa8reuibgi8hheru883', true), 
	('TransactionalBanker 2', 1111, 30000, 700, CURRENT_TIMESTAMP, 'TB 2', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true), 
	('TeamLead 1', 1111, 30000, 701, CURRENT_TIMESTAMP, 'TL 1', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true),
	('RelationshipManager 1', 1111, 30000, 702, CURRENT_TIMESTAMP, 'RM 1', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true),
	('TransactionalBanker 3', 2222, 50000, 700, CURRENT_TIMESTAMP, 'TB 3', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true),
	('TransactionalBanker 4', 2222, 50000, 700, CURRENT_TIMESTAMP, 'TB 4', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true),
	('TeamLead 2', 2222, 50000, 701, CURRENT_TIMESTAMP, 'TL 2', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true),
	('RelationshipManager 2', 2222, 50000, 702, CURRENT_TIMESTAMP, 'RM 2', 'surname', 'aoieorg93ythaiuhg8a97gawi74h', true)
	ON CONFLICT (username) DO NOTHING;
	
	
INSERT INTO warm.portfolio (portfolio_id, rm_username, team_id, created_at)
VALUES 
	(54321, 'RelationshipManager 1', 30000, CURRENT_TIMESTAMP), 
	(12345, 'RelationshipManager 2', 50000, CURRENT_TIMESTAMP)
	ON CONFLICT (portfolio_id) DO NOTHING;

INSERT INTO warm.customer (cus_id, portfolio_id, cus_mobile_number, created_at, updated_at, cus_sa_id, cus_tcs, cus_opt_status, cus_firstname, cus_middlename, cus_lastname)
VALUES 
	(10101, 54321, '27820889391', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Christo', null, 'Erasmas'),
	(20202, 54321, '27828525750', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Madira', null, 'Njomo'),
	(30303, 54321, '27842993112', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Jurie', null, 'Oberholzer'),
	(40404, 54321, '27743184606', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Ash', null, 'Ahmed'),
	(50505, 54321, '27798762340', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Darren', null, 'Knibbs'),
	(60606, 54321, '27767468618', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Bhutiza', null, 'Kubheka'),
	(70707, 54321, '27795297859', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Khutso', null, 'Mothapo'),
	(80808, 54321, '27823165916', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Robert JR', null, 'Zeilinga'),
	(90909, 54321, '27798056530', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Mulugisi', null, 'Dzilafho'),
	(101010, 12345, '27767468611', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 1', null, 'lastname'),
	(111111, 12345, '27795297852', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 2', null, 'lastname'),
	(121212, 12345, '27823165913', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 3', null, 'lastname'),
	(131313, 12345, '27798056534', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 4', null, 'lastname'),
	(141414, 54321, '27767468611', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 5', null, 'lastname'),
	(151515, 54321, '27795297852', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 6', null, 'lastname'),
	(161616, 54321, '27823165913', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 7', null, 'lastname'),
	(171717, 54321, '27798056531', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'customer 8', null, 'lastname')ON CONFLICT (cus_id) DO NOTHING;

INSERT INTO warm.internal_user_role (internal_user_role_id, username, role_id, created_at) 
	VALUES 
	(01111, 'TransactionalBanker 1', 001, CURRENT_TIMESTAMP), 
	(02222, 'TransactionalBanker 2', 002, CURRENT_TIMESTAMP), 
	(03333, 'TransactionalBanker 3', 001, CURRENT_TIMESTAMP) ON CONFLICT (internal_user_role_id) DO NOTHING;

INSERT INTO warm.conversation (cus_id, username, conversation_status, created_at)
VALUES 
(10101, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(20202, 'TransactionalBanker 2', 'ACTIVE', CURRENT_TIMESTAMP),
(30303, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(40404, 'TransactionalBanker 2', 'ACTIVE', CURRENT_TIMESTAMP),
(50505, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(60606, 'TransactionalBanker 2', 'ACTIVE', CURRENT_TIMESTAMP),
(70707, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(80808, 'TransactionalBanker 2', 'ACTIVE', CURRENT_TIMESTAMP),
(90909, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(101010, 'TransactionalBanker 3', 'ACTIVE', CURRENT_TIMESTAMP),
(111111, 'TransactionalBanker 4', 'ACTIVE', CURRENT_TIMESTAMP),
(121212, 'TransactionalBanker 3', 'ACTIVE', CURRENT_TIMESTAMP),
(131313, 'TransactionalBanker 4', 'ACTIVE', CURRENT_TIMESTAMP),
(141414, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(151515, 'TransactionalBanker 2', 'ACTIVE', CURRENT_TIMESTAMP),
(161616, 'TransactionalBanker 1', 'ACTIVE', CURRENT_TIMESTAMP),
(171717, 'TransactionalBanker 2', 'ACTIVE', CURRENT_TIMESTAMP)
ON CONFLICT (conversation_id) DO NOTHING;

INSERT INTO warm.message (msg_id, conversation_id, msg_sender_id, msg_text, msg_status, msg_timestamp, created_at)
VALUES 
	('a', 10, 101010, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('b', 10, 'TransactionalBanker 3', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('c', 11, 111111, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('d', 11, 'TransactionalBanker 4', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('e', 12, 121212, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('f', 12, 'TransactionalBanker 3', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('g', 13, 131313, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('h', 13, 'TransactionalBanker 4', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('i', 14, 141414, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('j', 14, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('k', 15, 151515, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('l', 15, 'TransactionalBanker 2', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('m', 16, 161616, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('n', 16, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('o', 17, 171717, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('p', 17, 'TransactionalBanker 2', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('q', 1, 10101, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('r', 1, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('s', 2, 20202, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('t', 2, 'TransactionalBanker 2', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('u', 3, 30303, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('v', 3, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('w', 4, 40404, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('x', 4, 'TransactionalBanker 2', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('y', 5, 50505, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('z', 5, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('aa', 6, 60606, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('bb', 6, 'TransactionalBanker 2', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('cc', 7, 70707, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('dd', 7, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('ee', 8, 80808, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('ff', 8, 'TransactionalBanker 2', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP),
	('gg', 9, 90909, 'Hello world', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP), 
	('hh', 9, 'TransactionalBanker 1', 'Hello world again', 'read', '2020-03-04 13:48:12', CURRENT_TIMESTAMP)
	ON CONFLICT (msg_id) DO NOTHING;
