ALTER TABLE warm.team
ADD team_prestige BOOLEAN;

UPDATE warm.team
SET team_prestige = false
WHERE team_id = 30000;

UPDATE warm.team
SET team_prestige = false
WHERE team_id = 50000;

INSERT INTO warm.team (team_id, team_prestige, created_at) 
VALUES 
	(40000, true, CURRENT_TIMESTAMP) 
    ON CONFLICT (team_id) DO NOTHING;

INSERT INTO warm.internal_user (username, cst_id, team_id, user_type_id, created_at, usr_name, usr_surname, usr_password, usr_status)
VALUES 
	('Prestige Banker 1', 1111, 40000, 700, CURRENT_TIMESTAMP, 'PB 1', 'surname', 'ofnf34ityhfoa8reuibgi8hheru883', true),
    ('Prestige Banker 2', 1111, 40000, 700, CURRENT_TIMESTAMP, 'PB 2', 'surname', 'ofnf34ityhfoa8reuibgi8hheru883', true),
    ('Team Lead PB', 1111, 40000, 702, CURRENT_TIMESTAMP, 'TL PB', 'surname', 'ofnf34ityhfoa8reuibgi8hheru883', true)
    ON CONFLICT (username) DO NOTHING;

INSERT INTO warm.portfolio (portfolio_id, rm_username, team_id, created_at)
VALUES 
	(1, 'Prestige Banker 1', 40000, CURRENT_TIMESTAMP),
    (2, 'Prestige Banker 2', 40000, CURRENT_TIMESTAMP)
    ON CONFLICT (portfolio_id) DO NOTHING;

INSERT INTO warm.customer (cus_id, portfolio_id, cus_mobile_number, created_at, updated_at, cus_sa_id, cus_tcs, cus_opt_status, cus_firstname, cus_middlename, cus_lastname)
VALUES 
	(3, 1, '27631733668', CURRENT_TIMESTAMP, null, 2001015800085, true, true, 'Tlotlo', null, 'Rakgahla')
    ON CONFLICT (cus_id) DO NOTHING;