CREATE TABLE portfolio
(
  portfolio_id SERIAL PRIMARY KEY,
  portfolio_description VARCHAR(50)
);
INSERT INTO portfolio (portfolio_description) VALUES ('aiih93u4vnjinvese2esij32'), ('q00qvvnn9evdvsiurh98rr2ij');

CREATE TABLE customer
(
    customer_id  BIGSERIAL PRIMARY KEY,
    portfolio_id BIGINT REFERENCES portfolio (portfolio_id),
    fk_user_id   BIGINT REFERENCES internal_user (user_id)
);
INSERT INTO customer (customer_id, portfolio_id) VALUES (10101, 1), (20202, 2);

CREATE TABLE user_type
(
  user_type_id SERIAL PRIMARY KEY,
  user_type VARCHAR(50)
);
INSERT INTO user_type (user_type) VALUES ('TRANSACTIONAL_BANKER'), ('TEAM_LEAD'), ('RELATIONSHIP_MANAGER');

CREATE TABLE role
(
  role_id SERIAL PRIMARY KEY,
  role_description VARCHAR(50)
);
INSERT INTO role (role_description) VALUES ('ADMIN'), ('SYSTEM_USER');

CREATE TABLE cst
(
  cst_id SERIAL PRIMARY KEY,
  cst_name VARCHAR(50)
);
INSERT INTO cst (cst_id, cst_name) VALUES (1111, 'Johannesburg'), (2222, 'Pretoria');

CREATE TABLE internal_user
(
  user_id SERIAL PRIMARY KEY,
  cst_id INT REFERENCES cst (cst_id),
  user_type_id INT REFERENCES user_type (user_type_id)
);
INSERT INTO internal_user (user_id, cst_id, user_type_id) VALUES (121212, 1111, 1), (131313, 2222, 2), (141414, 1111, 1);

CREATE TABLE internal_user_role
(
  user_role_id BIGSERIAL PRIMARY KEY,
  user_id BIGINT REFERENCES internal_user (user_id),
  role_id INT REFERENCES role (role_id)
);
INSERT INTO internal_user_role (user_id, role_id) VALUES (121212, 1), (131313, 2), (141414, 1);

CREATE TABLE team
(
  team_id SERIAL PRIMARY KEY,
  user_id INT REFERENCES internal_user (user_id),
  portfolio_id INT REFERENCES portfolio (portfolio_id)
);
INSERT INTO team (user_id, portfolio_id) VALUES (131313, 1);

CREATE TABLE conversation
(
  conversation_id BIGSERIAL PRIMARY KEY,
  customer_id BIGINT,
  user_id BIGINT REFERENCES internal_user (user_id),
  conversation_status VARCHAR(30),
  referral_id BIGINT REFERENCES internal_user (user_id)
);
INSERT INTO conversation (conversation_id, customer_id, user_id, conversation_status, referral_id) VALUES (101, 20000, 141414, 'OPEN', 131313);

CREATE TABLE message
(
  message_id VARCHAR(50) PRIMARY KEY,
  conversation_id BIGINT REFERENCES conversation (conversation_id),
  user_id BIGINT REFERENCES internal_user (user_id),
  message_text VARCHAR,
  local_date_time timeStamp
);
INSERT INTO message (message_id, conversation_id, user_id, message_text, local_date_time) VALUES ('3q4tknsldgnl09wnglns9ks', 101, 141414, 'Hello world', '2020-03-04 13:48:12');
