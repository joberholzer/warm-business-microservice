FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8081
ADD target/warm-business-service-0.0.1-SNAPSHOT.jar warm-business-service.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "warm-business-service.jar"]